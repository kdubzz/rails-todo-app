# Rails To-Do Classic

A Rails-on-Ruby take on the classic to-do app.
This app is meant as a collaboration project with a friend intende to learn Rails
and other technologies including Ruby/Rails, and others (e.g. Docker)

Other areas of technology include

* User authentication / authorization (using Devise)

* Bootstrap

* React

* More as app development continues

## Branching
- All development happens on `develop`, with feature and update issues branching from `develop`
- Once `develop` has been accepted as sufficient (in terms of app features and functionality),
  a merge is made onto `master`

## Technologies used

### Ruby Version
- Rails 6.1.0
  - See [Ruby docs](https://www.ruby-lang.org/en/downloads/) for official download/installation
    - version 2.7.2 should suffice
    - installation of Ruby should install Ruby gems (_needed to install Rails_)

- Rails versioning can be changed conveniently using [`rvm`](https://rvm.io/).

### Database initialization/creation
- This app uses `postgresql` as the database for development and testing
- To start the db server on OSX sytems, run `pg_ctl -D /usr/local/var/postgresql start`
- See [here](https://www.postgresql.org/download/) for Linux and Windows download/installation
- Once Rails has been successfully set up (and the postgresql server is running),
  - `cd` to the project directory
  - Run `rails db:create, db:migrate` to create the database and migrate to the latest migration

