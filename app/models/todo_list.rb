class TodoList < ApplicationRecord
  validates :title, presence: true, length: { minimum: 3 }
  validates :description, presence: true, length: { minimum: 3 }

  extend FriendlyId

  friendly_id :title, use: :slugged 

  has_many :todo_items, dependent: :delete_all

  belongs_to :user
end
