import { Controller } from 'stimulus';

export default class extends Controller {
  static get targets() {
    return [
      'title',
      'description',
      'titleCounter',
      'descriptionCounter',
    ];
  };

  countTitleCharacters(event) {
    let titleCharacters = this.titleTarget.value.length;

    this.titleCounterTarget.innerText = titleCharacters;
  };

  countDescriptionCharacters(event) {
    let descriptionCharacters = this.descriptionTarget.value.length;

    this.descriptionCounterTarget.innerText = descriptionCharacters;
  }
};

