module Users
  class TodoListsController < UsersController 
    before_action :set_todo_list, except: %i[index new create incomplete]

    def index
      if current_user
        @pagy, @todo_lists = pagy(current_user.todo_lists.order(created_at: :desc), items: 10)
        @today = Time.new.strftime('%d of %B, %Y')
      end
    end

    def new
      @todo_list = TodoList.new
    end

    def create
      @todo_list = current_user.todo_lists.new(todo_list_params)

      if @todo_list.save
        redirect_to todo_lists_path, notice: 'New Todo-List Successfully Created'
      else
        redirect_to new_todo_list_path, alert: 'Something went wrong...'
      end
    end

    def show; end

    def edit; end

    def incomplete
      @pagy, @incomplete_lists = pagy(current_user.todo_lists.where(completed: false).order(created_at: :desc), items:10) 
      @today = Time.new.strftime('%d of %B, %Y')
    end

    def update
      if @todo_list.update(todo_list_params)
        if @todo_list.completed == true
          redirect_to todo_lists_path, notice: 'Todo-List Marked Complete!'
        elsif @todo_list.completed == false
          redirect_to todo_lists_path, notice: 'Todo-List Marked Incomplete!'
        end
      else
        redirect_to edit_todo_list_path, notice: 'Something went wrong...'
      end
    end

    def destroy
      @todo_list.destroy

      redirect_to todo_lists_path, alert: 'List successfully deleted!'
    end

    private

    def set_todo_list
      @todo_list = TodoList.friendly.find(params[:id])
    end

    def set_incomplete_todo_list
      @incomplete_lists = TodoList.find_by(completed: false)
    end

    def todo_list_params
      params.require(:todo_list).permit(:title, :description, :completed)
    end
  end
end
