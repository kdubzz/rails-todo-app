module Users
  class TodoItemsController < UsersController 
    before_action :set_todo_list
    before_action :set_todo_item, except: [:create]

    def create
      @todo_item = @todo_list.todo_items.create(todo_item_params)

      if @todo_item.save
        redirect_to @todo_list, notice: 'New Task created!'
      else
        redirect_to new_todo_list_todo_item_path
      end
    end

    def update
      @todo_item.update_attribute(:completed, false)

      redirect_to @todo_list, notice: 'Task marked incomplete'
    end

    def destroy
      if @todo_item.destroy
        flash[:success] = 'Task successfully deleted.'
      else
        flash[:error] = 'Task cannot be deleted'
      end

      redirect_to @todo_list, notice: 'Task Deleted!'
    end

    def completed
      @todo_item.update_attribute(:completed, true)

      redirect_to @todo_list, notice: 'Task Completed!'
    end

    private

    def set_todo_item
      @todo_item = @todo_list.todo_items.find(params[:id])
    end

    def set_todo_list
      @todo_list = TodoList.friendly.find(params[:todo_list_id])
    end

    def todo_item_params
      params.require(:todo_item).permit(:description)
    end
  end
end
