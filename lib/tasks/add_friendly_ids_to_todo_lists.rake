desc 'Adding Friendly Id slugs to TodoLists'

task todo_list: :environment do
  puts 'Adding friendly-id slugs to todo lists'
  TodoList.find_each(&:save)
end
