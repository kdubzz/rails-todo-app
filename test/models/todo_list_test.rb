require 'test_helper'

class TodoListTest < ActiveSupport::TestCase
  test "todo lists should exist" do
    todo_list = TodoList.new
    assert todo_list, true
  end

  test "todo lists should not save without a title or description" do
    todo_list = TodoList.new
    assert_not todo_list.save
  end

  test "todo lists should not save with only a title attribute" do
    todo_list = TodoList.new(title: "todo list title")
    assert_not todo_list.save
  end

  test "todo lists should not save with only a description attribute" do
    todo_list = TodoList.new(description: "todo list description")
    assert_not todo_list.save
  end

  test "todo lists should not save with invalid attributes" do
    todo_list = TodoList.new(
      title: "",
      description: ""
    )
    assert_not todo_list.save
  end
end
