require 'test_helper'

class TodoItemTest < ActiveSupport::TestCase
  test "todo item should exist" do
    todo_item = TodoItem.new
    assert todo_item, true
  end

  test "todo item should not save with no attributes" do
    todo_item = TodoItem.new
    assert_not todo_item.save
  end

  test "todo item should not save with invalid attributes" do
    todo_item = TodoItem.new(description: "")
    assert_not todo_item.save
  end
end
