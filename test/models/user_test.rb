require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "user should exist" do
    user = User.new
    assert user, true
  end

  test "user should not save without an email or password" do
    user = User.new
    assert_not user.save
  end

  test "user should not save with only an email attribute" do
    user = User.new(email: "test@example.com")
    assert_not user.save
  end

  test "user should not save with only a password attribute" do
    user = User.new(encrypted_password: "3793874kldjfi")
    assert_not user.save
  end

  test "user should not save with invalid attributes" do
    user = User.new(
      email: "",
      encrypted_password: ""
    )
    assert_not user.save
  end
end
