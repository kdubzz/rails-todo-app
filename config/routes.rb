Rails
  .application
  .routes
  .draw do
    devise_for :users

    scope module: :users do
      resources :todo_lists do
        resources :todo_items do
          member { patch :completed }
        end
      end

      get 'incomplete', to: 'todo_lists#incomplete'
    end

    root 'users/todo_lists#index'
  end
