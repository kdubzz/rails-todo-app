class AddCompletedToTodoList < ActiveRecord::Migration[6.1]
  def change
    add_column :todo_lists, :completed, :boolean, default: false
  end
end
